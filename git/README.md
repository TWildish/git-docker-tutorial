# Now what do I do? #

Just read the exercises in order. You can cut and paste the commands there, for the most part, but it's better if you type them yourself.

For further information, your best bets are:

- google
- 'Pro Git' by Scott Chacon. You can find that for free at [https://git-scm.com/book/en/v2](https://git-scm.com/book/en/v2).
- 'Git Magic' by Brian Lynn, available at [http://www-cs-students.stanford.edu/~blynn/gitmagic/](http://www-cs-students.stanford.edu/~blynn/gitmagic/)
- or you can contact your consultants, who will use google and those books on your behalf :-)
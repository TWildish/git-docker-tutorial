# Now what? #

Follow the exercises in order!

For a practical look at real-world containers used for bioinformatics, check out [http://bioboxes.org/](http://bioboxes.org/)

For more information, there are plenty of good tutorials out there. Two good ones are:

- [https://prakhar.me/docker-curriculum/](https://prakhar.me/docker-curriculum/)
- [https://docs.docker.com/engine/tutorials/](https://docs.docker.com/engine/tutorials/)

There's also one which shows how to build an image for MEGAHIT interactively:

- [https://github.com/ngs-docs/2015-nov-docker/blob/master/docker-intro.rst](https://github.com/ngs-docs/2015-nov-docker/blob/master/docker-intro.rst)

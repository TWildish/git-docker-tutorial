## Objective ##
Having run velvet in a docker container, re-run it on Cori, under shifter.

### Running under shifter ###
Shifter provides much of the functionality needed to run docker images, but in a restricted way, suitable for our Cray environment. You can read more about shifter at [http://www.nersc.gov/users/software/using-shifter-and-docker/](http://www.nersc.gov/users/software/using-shifter-and-docker/).

The steps are:

- log in to cori, **cd** to your scratch directory (if you have one)
- download this git repository, **cd** into the directory with the bioboxes inputs
- download the docker image for **bioboxes/velvet**, so shifter can find it
- grab an interactive node, and run the image

```
# get the tutorial code and unpack it
> git clone https://TWildish@bitbucket.org/TWildish/git-docker-tutorial.git
> cd git-docker-tutorial/docker/bioboxes

# if git doesn't work for whatever reason, do this instead
# wget https://bitbucket.org/TWildish/git-docker-tutorial/get/master.zip
# unzip master.zip
# cd TWildish-git-docker-tutorial-*/docker/bioboxes

# fetch the docker image
# 'shifterimg' provides some of the functionality of the 'docker' command, for managing images
> shifterimg pull bioboxes/velvet

# request an interactive login to a batch node
> salloc -N 1 -p debug -C haswell
```

Once you have your batch node, run the image. Note that we simplify the mount-points (the ```--volume``` options) to a single mount, and we invoke the **assemble.sh** script which is in our repository, and available to the container through that mount-point. If you look at **assemble.sh** you'll see it's quite trivial, just directly invoking velvet on the files we attach through the mountpoint.

This is a reasonable generic model for using a container: provide the binaries in the container, and the data+driver-script through a volume that you attach, then invoke the driver inside the container. The downside of it is that the driver script and the container need to be maintained together, but hey, that's what git repositories are for. Just keep the Dockerfile with the git repository and you should have no problems.

```
# the 'shifter' command provides functionality to actually run docker images
> srun -N 1 -n 1 shifter --volume=`pwd`:/bbx --image=bioboxes/velvet /bbx/assemble.sh
[0.000000] Reading FastQ file /bbx/input_data/reads.fq.gz;
[0.005190] 228 sequences found
[0.005192] Done
[...]
```

Note that instead of providing the argument 'default', as we did in the third exercise, we have to provide a script that does what we want manually. This is a consequence of the restricted environment that shifter provides, and the way that affects containers depending on how they're built. This will vary case-by-case, if there are any general rules for it I don't know them yet.

Anyway, if that worked, you should see the output files in the **output_data** directory.

```
> ls -l output_data/
total 192
-rw-r--r--  1 wildish  wildish   6529 Nov 30 22:02 Graph
-rw-r--r--  1 wildish  wildish   5447 Nov 30 22:02 LastGraph
-rw-r--r--  1 wildish  wildish   3336 Nov 30 22:02 Log
-rw-r--r--  1 wildish  wildish   4727 Nov 30 22:02 PreGraph
-rw-r--r--  1 wildish  wildish   9231 Nov 30 22:02 Roadmaps
-rw-r--r--  1 wildish  wildish  45610 Nov 30 22:02 Sequences
-rw-r--r--  1 wildish  wildish   2812 Nov 30 22:02 contigs.fa
-rw-r--r--  1 wildish  wildish    160 Nov 30 22:02 stats.txt
```

### Conclusion ###
Shifter adds some restrictions to what you can do with a docker image, mainly for reasons of security and playing nicely in the cray environment.

With minor modifications, existing container images can be adapted to work with shifter, as well as with vanilla docker.

If you build images from scratch, you can make sure they work in both environments out of the box.

### Best Practice ###

- start simple, practice with shifter, and learn about the limitations it imposes
- consider separating the execution of the binaries from the startup of the container, trading flexibility with maintainability
- try your containers in multiple environments (laptop, shifter, Amazon, Google) if you want real portability